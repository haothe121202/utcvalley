using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingCutscene : MonoBehaviour
{
    public string mainMenu;
    public string restartGame;


    public void Setup()
    {
        gameObject.SetActive(true);
    }

    public void RestartButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("restartMenu");
    }

    public void ExitButton()
    {
        SceneManager.LoadScene("mainMenu");
    }
}
