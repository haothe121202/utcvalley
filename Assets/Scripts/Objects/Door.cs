using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorType
{
    key,
    enemy,
    button,
    masterkey
}
public class Door : Interacable
{
    [Header("Door variables")]
    public DoorType thisDoorType;
    public bool open = false;
    public Inventory playerInventory;
    public SpriteRenderer doorSprite;
    public BoxCollider2D physicsCollider;
    private void Start()
    {
        doorSprite = GetComponent<SpriteRenderer>();
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (playerinRange && thisDoorType == DoorType.key)
            {

                //Does the player have a key
                if (playerInventory.numberOfKeys > 0)
                {
                    //If so, then call the open method
                    playerInventory.numberOfKeys--;
                    Open();
                }

            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (playerinRange && thisDoorType == DoorType.masterkey)
            {

                //Does the player have a key
                if (playerInventory.numberOfMTKeys > 0)
                {
                    //If so, then call the open method
                    playerInventory.numberOfMTKeys--;
                    Open();
                }

            }
        }
    }
    public void Open()
    {
        //Turn off the door'sPrite
        doorSprite.enabled = false;
        //set open to true
        open = true;
        //turn off the door's  box collider
        physicsCollider.enabled = false;
    }
    public void Close()
    {
        doorSprite.enabled = true;
        open = false;
        physicsCollider.enabled = true;
    }
}
