using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene("Opening Cutscene");
    }

    public void HowToPlay()
    {
        SceneManager.LoadScene("Guide");
    }

    public void ExitToDesktop()
    {
        Application.Quit();
    }

    public void StartMenu() {
        SceneManager.LoadScene("StartMenu");
    }

}
