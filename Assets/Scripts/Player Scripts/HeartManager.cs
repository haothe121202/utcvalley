using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HeartManager : MonoBehaviour
{
    public Image[] hearts;

    public Sprite fullHeart;
    public Sprite halffullHeart;
    public Sprite emptyHeart;
    public FloatValue heartContainers;
    public FloatValue playerCurrentHeart;
    public GameOverScreen gameOverScreen;
    // Start is called before the first frame update

    public void Reset() {
        playerCurrentHeart = heartContainers;
    }

    void Start()
    {
        InitHeart();
    }
    public void InitHeart()
    {
        for (int i = 0; i < heartContainers.initialValue; i++)
        {
            hearts[i].gameObject.SetActive(true);
            hearts[i].sprite = fullHeart;
        }
    }

    public void UpdateHearts()
    {
        float tempHealth = playerCurrentHeart.RuntimeValue / 2;
        for (int i = 0; i < heartContainers.initialValue; i++)
        {
            if (i <= tempHealth - 1)
            {
                //fullheart
                hearts[i].sprite = fullHeart;
            }
            else if (i >= tempHealth)
            {
                //empty heart
                hearts[i].sprite = emptyHeart;
            }
            else
            {
                //halffull hearts
                hearts[i].sprite = halffullHeart;
            }
        }
        if(tempHealth <= 0) {
            AudioManager.Instance.PlaySFX("Lose");
            AudioManager.Instance.PlaySFX("Lose");
            playerCurrentHeart.RuntimeValue = 6;
            Time.timeScale = 0f;
            gameOverScreen.Setup();
        }
    }
}
